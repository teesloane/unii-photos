import arthur from './arthur.png'
import arthurHover from './arthur-hover.png'
import alex from './alex.png'
import alexHover from './alex-hover.png'
import adam from './adam.png'
import adamHover from './adam-hover.png'
import josh from './josh.png'
import joshHover from './josh-hover.png'
import jenny from './jenny.png'
import jennyHover from './jenny-hover.png'
import meghan from './meghan.png'
import meghanHover from './meghan-hover.png'
import marissa from './marissa.png'
import marissaHover from './marissa-hover.png'
import matt from './matt.png'
import mattHover from './matt-hover.png'
import seb from './seb.png'
import sebHover from './seb-hover.png'
import shiera from './shiera.png'
import shieraHover from './shiera-hover.png'
import tammy from './tammy.png'
import tammyHover from './tammy-hover.png'
import tyler from './tyler.png'
import tylerHover from './tyler-hover.png'

const people = [
  {
    name: "arthur",
    main_image: arthur,
    alt_image: arthurHover,
    tags: ["toronto"]
  },
  {
    name: "adam",
    main_image: adam,
    alt_image: adamHover,
    tags: ["toronto"]
  },
  {
    name: "josh",
    main_image: josh,
    alt_image: joshHover,
    tags: ["toronto"]
  },
  {
    name: "jenny",
    main_image: jenny,
    alt_image: jennyHover,
    tags: ["toronto"]
  },
  {
    name: "meghan",
    main_image: meghan,
    alt_image: meghanHover,
    tags: ["new york"]
  },
  {
    name: "marissa",
    main_image: marissa,
    alt_image: marissaHover,
    tags: ["toronto"]
  },
  {
    name: "matt",
    main_image: matt,
    alt_image: mattHover,
    tags: ["toronto"]
  },
  {
    name: "seb",
    main_image: seb ,
    alt_image: sebHover ,
    tags: ["new york"]
  },
  {
    name: "shiera",
    main_image: shiera ,
    alt_image: shieraHover ,
    tags: ["toronto"]
  },
  {
    name: "tammy",
    main_image: tammy,
    alt_image: tammyHover,
    tags: ["toronto"]
  },
  {
    name: "alex",
    main_image: alex,
    alt_image: alexHover,
    tags: ["toronto"]
  },
  {
    name: "tyler",
    main_image: tyler,
    alt_image: tylerHover,
    tags: ["toronto"]
  },
];

export default people
