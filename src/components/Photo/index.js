import React from "react";
import "./style.css";

export const Photo = props => {
  return (
    <section className="Photo__box">
      <img className="Photo" alt="steve" src="http://www.fillmurray.com/g/300/400" />
    </section>
  );
};
