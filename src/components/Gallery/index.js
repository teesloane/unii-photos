import React, { Component } from "react";
import ReactGridLayout from "react-grid-layout";
import ResponsiveReactGridLayout from 'react-grid-layout'
import "./style.css";

import { Photo } from "../Photo/index";
import people from '../../images';
import {shuffle} from '../../helpers/shuffle';

class Gallery extends Component {


  componentWillMount() {
    this.createStyleSheet()
  }

  createStyleSheet() {
    let styleElement = document.createElement("style");
    let stylesheet = styleElement.sheet;
    let styleId = document.createAttribute("id");

    styleId.value = "unii_styles"
    styleElement.setAttributeNode(styleId);
    document.head.appendChild(styleElement);
  }

  createClassForEachPerson(person, yOffset) {
    let styleSheet = document.getElementById('unii_styles')
    styleSheet.innerHTML += `
      .${person.name} {
        background-image: url(${person.main_image});
        background-size: 299px 397px;
        background-repeat: no-repeat;
        background-position: center ${yOffset}px ;
        background-position: center;
        border-radius: 12px;
        margin: 20px 0;
        // margin: 100px 0;
      }

      .${person.name}:hover {
        background-image: url(${person.alt_image})
      }
    `
  }


  renderPhotos() {
    let shuffledPeople = shuffle(people);

    let filteredPeople = shuffledPeople.filter(p => {
      p.tags.forEach(tag => {
        console.log(tag, this.props.filter)
        if (tag === this.props.filter) {
          return true
        }
      })
    })

    // console.log(filteredPeople, 'filtered people')
    const layout = shuffledPeople.map((photo, index) => {
      return {
        i: index.toString(),
        x: index * 3 % 12,
        y: Math.floor(index / 6) * index,
        w: 3,
        h: 11,
      };
    });

    const photos = people.map((photo, index) => {

      // set the background position based on even / odd numbers
      let backgroundYOffset = index % 2 === 0 ? 25 : 0
      console.log(backgroundYOffset)
      this.createClassForEachPerson(photo, backgroundYOffset)

      return (
        <div
          className={`gallery__block ${photo.name}`}
          key={index}
        />
      );
    });

    return (
      <ResponsiveReactGridLayout
        className="layout"
        layout={layout}
        cols={12}
        rowHeight={28}
        width={1300}
        isDraggable={false}
        isResizable={false}
      >
        {photos}
      </ResponsiveReactGridLayout>
    );
  }

  render() {
    return (
      <div className="Gallery__main">
        {this.renderPhotos()}
      </div>
    );
  }
}

export default Gallery;
