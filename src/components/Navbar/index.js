import React from "react";
import "../Skeleton/style.css"
import "./style.css";

export const Navbar = () => {
  const filterOptions = [
    "New York team",
    "Toronto team",
    "Developers",
    "Designers",
    "Bitcoin owners",
    "Cat owners"
  ];

  const renderSelectOptions = () =>
    filterOptions.map(o => <option value={o}>{o}</option>);

  return (
    <header className="Navbar__main">
      <div className="Navbar__dropdown">
        <p className="Navbar__p"> Show me </p>
        <select className="Navbar__select">
          {renderSelectOptions()}
        </select>
      </div>
      <div className="Navbar__links">
        <ul className="Navbar__links--list">
          <a href="https://www.universe.com/about"><li>About</li></a>
          <a href="https://careers.universe.com"><li>Careers</li></a>
        </ul>
      </div>

    </header>
  );
};
