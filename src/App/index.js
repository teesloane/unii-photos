import React, { Component } from "react";

// styles
import "./style.css";
import "../../node_modules/react-grid-layout/css/styles.css";
import "../../node_modules/react-resizable/css/styles.css";

// components
import { Navbar } from "../components/Navbar/index";
import Gallery from "../components/Gallery/index";

class App extends Component {
  state = {
    scrollY: 0,
    autoScroll: true,
    scrollSpeed: 30,
    infiniteScroll: true,
    filter: 'new york'
  };

  // Scrolling methods

  autoScroll() {
    if (this.state.autoScroll) {
      const autoScroll = () => window.scrollBy(0, 1);
      setInterval(autoScroll, this.state.scrollSpeed);
    }
  }

  infiniteScroll() {
    if (this.state.infiniteScroll) {
      document.addEventListener("scroll", e => {
        let scrollHeight = document.body.scrollHeight;
        let scrollYPos = window.pageYOffset + window.innerHeight;

        if (scrollHeight === scrollYPos) {
          window.scrollTo(0, 0);
        }
      });
    }
  }

  // filter stuff
  handleFilterChange(e) {
    console.log(e.target.value)
    this.setState({
      peopleFilter: ''
    })
  }

  render() {
    this.autoScroll();
    this.infiniteScroll();

    return (
      <div className="App">
        <Navbar />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
        <Gallery filter={this.state.filter} />
      </div>
    );
  }
}

export default App;
